<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShortUserController extends Controller
{
    public function index()
    {
        $links = auth()->user()->links()->paginate(6);

        return view('links.index', compact('links'));
    }


    public function home(){

        $links = auth()->user();

        return view('welcome', compact('links'));
    }
}
