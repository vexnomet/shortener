<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Link;


class LinkController extends Controller
{
    //
    //Display the Blade View
    public function Link()

    { 
       $links = Link::sortable()->paginate(7);
       // $links = auth()->user()->links()->paginate(6);
       //$links = Link::sortable()->paginate(5);
        return view('dashboard',compact('links'));
    }
}
