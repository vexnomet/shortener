<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShortRequest;
use Illuminate\Http\Request;
use App\Models\ShortUrl;
use App\Models\User;
use Carbon\Carbon;

class ShortUrlController extends Controller
{
    public function short(ShortRequest $request, $expiration_days = null)
    {
       if ($request->original_url)
       {
            $expiration_days = request('expiration_days');
            $link_name = $request->input('link_name');

            if (auth()->user())
            {
                $newUrl = auth()->user()->links()->create
                ([
                    'original_url' => $request->original_url
                ]);
                $newUrl->name = auth()->user()->name;

                if ($link_name) {
                    $newUrl->link_name = $link_name;
                }

                $newUrl->save();
            }

            else
            {
                $newUrl = ShortUrl::create
                ([
                    'original_url' => $request->original_url
                ]);

            }

            if ($expiration_days !== null)
            {
                $expire_date = Carbon::now()->addDays($expiration_days);
                $newUrl->expire_date = $expire_date;
            }

            if ($newUrl->expire_date === null)
            {
                $expiration_days = 7; // set the number of days the short URL should expire after
                $expire_date = Carbon::now()->addDays($expiration_days);
                $newUrl->expire_date = $expire_date;
            }

            if ($newUrl)
            {
                $short_url = base_convert($newUrl->id, 10,36);
                $newUrl->update
                ([
                    'short_url' => $short_url
                ]);

                return redirect()->back()->with('success_message', 'Your Short URL is Ready: <a class="text-green-500" href="'. url($short_url) .'">'. url($short_url) .'</a>' );
            }
       }
       return back();
    }


    public function shorten(ShortRequest $request)
    {
        // Validate the request data
        $validatedData = $request->validate([
            'original_url' => 'required|url',
            'link_name' => 'nullable|string|max:10|unique:short_urls,link_name',
            'expiration_days' => 'nullable|integer|min:0'
         ]);

        // Create a new ShortUrl instance
        $shortUrl = new ShortUrl();
        $shortUrl->original_url = $validatedData['original_url'];
        $shortUrl->link_name = $validatedData['link_name'];
        $shortUrl->user_id = $request->user()->id;
        $shortUrl->name = $request->user()->name;

        // If the expiration days are provided, set the expire_date field
        if (isset($validatedData['expiration_days']))
        {
            $shortUrl->expire_date = Carbon::now()->addDays($validatedData['expiration_days']);
        }

        // Save the ShortUrl instance
        $shortUrl->save();

        // Generate the short URL code
        $short_url = base_convert($shortUrl->id, 10, 36);
        $shortUrl->short_url = $short_url;
        $shortUrl->save();

        // Return the short URL as a response
        return response()->json([
            'short_url' => url($short_url),
            'created_by' => $request->user()->name,
            'created_at' => $shortUrl->created_at,
            'expire_date' => $shortUrl->expire_date
        ]);
    }


    public function index(Request $request)
    {
        // Get all short URLs created by the authenticated user
        $shortUrls = $request->user()->links()->get();

        // Return the short URLs as a response
        return response()->json($shortUrls);
    }

    public function show($code)
    {
        $shortUrl = ShortUrl::where('link_name', $code)->first();

         // If it's not a link name, check if it's a short URL code
        if (!$shortUrl)
        {
            $shortUrl = ShortUrl::where('short_url', $code)->first();
        }

        if ($shortUrl && !$shortUrl->expired)
        {
            $shortUrl->increment('visits');
            return redirect()->to(url($shortUrl->original_url));
        }

        else
        {
            return view('error', ['error' => 'This short URL has expired']);
        }
    }
}
