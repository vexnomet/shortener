<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Models\ShortUrl;

class ExpireShortUrls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */



    protected $signature = 'short:expire';


    protected $description = 'Expire short URLs that have reached their expiration date';

    /**
     * The console command description.
     *
     * @var string
     */

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ShortUrl::where('expire_date', '<', Carbon::now())
            ->update(['expired' => true]);
    }
}
