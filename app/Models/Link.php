<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Link extends Model
{
    use HasFactory,Sortable;

  protected $table = 'short_urls';
    protected $fillable = [ 'link_name' ];
    
    public $sortable = ['user_id', 'name', 'original_url', 'short_url', 'visits', 'expire_date', 'expired','link_name'];
}
