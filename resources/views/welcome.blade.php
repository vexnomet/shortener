<x-guest-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 flex justify-center bg-white border-b border-gray-200">
                    <section>
                        <h1 class="text-4xl text-blue-800">Short Your Link</h1>
                        @if (session('success_message'))
                            {!! session('success_message') !!}
                        @endif
                        <form method="POST" action="{{ route('short.url') }}" style="text-align: center">
                            @csrf
                            <input class="border border-gray-300 rounded-lg" type="url" name="original_url" />
                            <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label for="link_name" ><h3>Link Name</h3></label><br>
                            <input class="border border-gray-300 rounded-lg" type="text" name="link_name" maxlength="10" />
                            <br>
                            <button class="m-2 px-6 py-2 bg-green-500 hover:bg-green-700 rounded-lg" type="submit">Short</button>
                            <br>
                            <label for="expiration_days">Expiration (in days)</label>
                            <select id="expiration_days" name="expiration_days">
                                <option value="1">1 day</option>
                                <option value="7">7 days</option>
                            </select>
                            <br>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
