


<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>


 <main>


<!--
    <div class="max-w-6xl mx-auto mt-8">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
              <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-200">
                    <thead class="bg-gray-50 dark:bg-gray-600 dark:text-gray-200">
                      <tr>
                        <th scope="col" colspan="3"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider" >Most</th></tr>
                        <tr>
                        <th scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">User Name</th>
                        <th scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">Visit Url</th>
                        <th scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">Short URL</th>
                      </tr>

                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                      <tr>
                      <td>sdfghj</td>
                      <td>sdfghj</td>
                      <td>sdfgh</td>
                      </tr>

                    </tbody>
                  </table>


                <div style="height: 50px"></div>
                <table class="min-w-full divide-y divide-gray-200">
                    <thead class="bg-gray-50 dark:bg-gray-600 dark:text-gray-200">
                      <tr>
                        <th scope="col" colspan="3"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider" >Most</th></tr>
                        <tr>
                        <th scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">User Name</th>
                        <th scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">Visit Url</th>
                        <th scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">Short URL</th>
                      </tr>

                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                      <tr>
                      <td>sdfghj</td>
                      <td>sdfghj</td>
                      <td>sdfgh</td>
                      </tr>

                    </tbody>
                  </table>



                  <div style="height: 50px"></div>
                  <table class="min-w-full divide-y divide-gray-200">
                      <thead class="bg-gray-50 dark:bg-gray-600 dark:text-gray-200">
                        <tr>
                          <th scope="col" colspan="3"
                          class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider" >Most</th></tr>
                          <tr>
                          <th scope="col"
                          class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">User Name</th>
                          <th scope="col"
                          class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">Visit Url</th>
                          <th scope="col"
                          class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">Short URL</th>
                        </tr>

                      </thead>
                      <tbody class="bg-white divide-y divide-gray-200">
                        <tr></tr>
                        <tr>
                            <td>sdfghj</td>
                            <td>sdfghj</td>
                            <td>sdfgh</td>
                            </tr>
                      </tbody>
                    </table>

                </div>
              </div>
            </div>
        </div>

    </div>  -->

    <div class="max-w-6xl mx-auto mt-8">
      <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">

    <table class="table table-bordered">
      <thead>
          <tr>

            <th scope="col"
            class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">@sortablelink('Name')</th>

            <th scope="col"
            class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">@sortablelink('Short Url')</th>
            <th scope="col"
            class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">@sortablelink('Visit Count')</th>
            <th scope="col"
            class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">@sortablelink('Expire Date')</th>

          </tr>
      </thead>
      <tbody>
          @if($links->count())
              @foreach($links as $key => $link)
                  <tr>

                      <td>{{ $link->name }}</td>

                      <td>{{ url($link->short_url) }}</td>
                      <td>{{ $link->visits }}</td>
                      <td>{{ $link->expire_date }}</td>

                  </tr>
              @endforeach
          @endif
      </tbody>
  </table>
  {!! $links->appends(\Request::except('page'))->render() !!}
</div>

  </div>


</div>
</div>

</div>

 </main>

</x-app-layout>
