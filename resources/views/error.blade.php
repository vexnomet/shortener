@if (isset($error))
    <div class="bg-red-500 text-white py-2 px-3 rounded-md mt-3">
        <h1 class="text-2xl font-bold">{{ $error }}</h1>
    </div>
@endif
