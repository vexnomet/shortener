<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShortUrlController;
use App\Http\Controllers\ShortUserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

Route::get('/', [ShortUserController::class, 'home'])->name('welcome')->middleware('auth');
//Route::get('/links', 'App\Http\Controllers\LinkController@Link')->name('user.links')->middleware('auth');
Route::get('/dashboard', 'App\Http\Controllers\LinkController@Link')->name('dashboard')->middleware('auth');


Route::get('/links', [ShortUserController::class, 'index'])->name('user.links')->middleware('auth');
Route::post('/short', [ShortUrlController::class, 'short'])->name('short.url')->middleware('auth');
Route::get('/{code}', [ShortUrlController::class, 'show'])->name('short.show');
Route::get('/error', function () {
    if (session('error')) {
        return view('error');
    } else {
        return redirect()->to(url('/'));
    }
})->name('error');
